from __future__ import annotations
from typing import List, Dict
from abc import ABC, abstractmethod
import random

from tag import Suit, Color, RowType, CardType
from ability import AbilityTag, StatusTag


class gameObj(ABC):
    def __init__(self):
        self._cards = []

    #@abstractmethod
    def update(self):
        pass

    def cardList(self):
        return self._cards[:]


class Board(gameObj):
    def __init__(self, players: List[Player], decks: List[CardPile], hands: List[CardPile], graves: List[CardPile], fields):
        super().__init__()
        self._currentPlayer: int = 0
        self._players = players
        self._decks = decks
        self._hands = hands
        self._graves = graves
        self._fields = fields

    def addPlayer(self, *players):
        for player in players:
            self._players.append(player)

    def getPlayer(self, Id):
        return self._players[Id]

    def getDeck(self, Id):
        return self._decks[Id]

    def getHand(self, Id):
        return self._hands[Id]

    def getGrave(self, Id):
        return self._graves[Id]

    def getField(self, Id):
        return self._fields[Id]

    @property
    def currentPlayer(self):
        return self._currentPlayer


class Player(gameObj):
    _playerNum = 0

    def __new__(cls, *args):
        obj = object.__new__(cls)
        obj._playerID = cls._playerNum
        cls._playerNum += 1
        return obj

    def __init__(self, max_life: int, init_life: int):
        self._maxLife = max_life
        self._life = init_life

    def __str__(self):
        return f"Player ID: {self._playerID}, health: {self.life}/{self._maxLife}"

    @property
    def maxLife(self):
        return self._maxLife

    @property
    def life(self):
        return self._life

    @life.setter
    def life(self, life: int):
        self._life = life


class CardPile(gameObj):
    def __init__(self, *cards):
        super().__init__()
        self._cards = list(cards)

    def addTop(self, card: GwentCard):
        self._cards.append(card)
        self.update()

    def addBottom(self, card: GwentCard):
        self._cards.insert(0, card)
        self.update()

    def insert(self, index: int, card: GwentCard):
        self._cards.insert(index, card)
        self.update()

    def pop(self, index: int):
        out = self._cards.pop(index)
        self.update()
        return out

    def shuffle(self):
        random.shuffle(self._cards)
        self.update()

    def getIndex(self, card: GwentCard):
        return self._cards.index(card)

    def update(self):
        super().update()
        for card in self._cards:
            card.update()

    @property
    def cardList(self):
        return self._cards[:]


class Card():
    _instances = {}
    _newId: int = 0

    @classmethod
    def get(cls, cardId: int):
        return cls._instances[cardId]

    @classmethod
    def getInstances(cls) -> dict:
        return cls._instances.copy()
    
    def __init__(self, suit: Suit, num: int):
        # Add instance to cls._instance
        type(self)._instances[type(self)._newId] = self

        # Set cardId
        self._cardId = type(self)._newId

        self._suit = suit
        self._num = num

        # Incriment newId
        type(self)._newId += 1

    def __str__(self):
        return f"{self._suit}{self._num}"

    @property
    def num(self) -> int:
        return self._num

    @property
    def suit(self) -> Suit:
        return self._suit

    @property
    def color(self) -> Color:
        return self._suit.color

    @property
    def cardId(self) -> int:
        return self._cardId


class GwentCard(gameObj):
    def __init__(self, card: Card, card_type: CardType, abilities = []):
        super().__init__()
        self._origCardId: int = card.cardId
        self._suit: Suit = card.suit
        self._num: int = card.num
        self._playableRow: List[RowType] = [RowType((card.num + 1) % 3 + 1)]
        self._abilities = abilities
        self._cardType: CardType = card_type

        if self._cardType == CardType.UNIT:
            self._basePower = self._num
        else:
            self._basePower = 0
        # Set default power
        self._power = self._basePower
        self._defaultBasePower = self._basePower

    def __str__(self):
        abl = ""
        for ability in self._abilities:
            if ability in StatusTag:
                abl += f"{ability}"
        return f"{self._suit}{self._num}[{abl}]"

    def initialize(self):
        self._playableRow = RowType((self._num + 1) % 3 + 1)

        if self._cardType == CardType.UNIT:
            self._basePower = self._num
        else:
            self._basePower = 0
        # Reset power
        self._power = self._basePower

        for i, ability in enumerate(self._abilities):
            if ability in StatusTag:
                del self._abilities[i]

    def addStatus(self, status: StatusTag):
        self._abilities.append(status)

    def rmStatus(self, statusTag: StatusTag):
        for i, status in enumerate(self._abilities):
            if status == statusTag:
                del self._abilities[i]

    @property
    def playableRow(self):
        return self._playableRow.copy()

    @property
    def origCardId(self):
        return self._origCardId

    @property
    def defaultBasePower(self):
        return self._defaultBasePower

    @property
    def basePower(self):
        return self._basePower

    @basePower.setter
    def basePower(self, power: int):
        self._basePower = power
        self.update()

    @property
    def power(self):
        return self._power

    @power.setter
    def power(self, power: int):
        self._power = power
        self.update()

    def addPlayableRow(self, row: RowType):
        if not row in self._playableRow:
            self._playableRow.append(row)

    def rmPlayableRow(self, row: RowType):
        for i, row in enumerate(self._playableRow):
            del self._playableRow[i]

    def getAbilities(self):
        return self._abilities[:]