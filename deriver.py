from __future__ import annotations

from enum import Enum, auto
from game_element import Board, gameObj, Player, CardPile
from tag import Party


class BoardElement(Enum):
    def __call__(self, party: Party):
        return _BoardElementWrap(self, party)

    Player = auto()

    Deck = auto()
    Grave = auto()
    Hand = auto()

    Field = auto()
    Row = auto()


class _BoardElementWrap():
    def __init__(self, tag: BoardElement, party: Party):
        self._tag = tag
        self._party = party

    @property
    def CardList(self):
        return _AtrListWrap(AtrList.CardList, self)

    @property
    def _tag_(self)->BoardElement:
        return self._tag

    def instance(self, board: Board, *args):
        _id = [[0, 1], [1, 0]]
        _player_ID = _id[board.currentPlayer][int(self._party)]
        return ELEMENT_GETTER.get(self._tag_)(board, _player_ID)


class AtrList(Enum):
    CardList = auto()


class _AtrListWrap():
    def __init__(self, atr_tag: AtrList, elem_tag: _BoardElementWrap):
        self._atr = atr_tag
        self._elem = elem_tag

    def instance(self, board: Board):
        return ATTRIBUTE_GETTER.get(self._atr)(self._elem.instance(board))


ELEMENT_CLASS = {
    BoardElement.Player: Player,
    BoardElement.Deck: CardPile,
    BoardElement.Grave: CardPile,
    BoardElement.Hand: CardPile,
    BoardElement.Field: gameObj
}


ELEMENT_GETTER = {
    BoardElement.Player: Board.getPlayer,
    BoardElement.Deck: Board.getDeck,
    BoardElement.Grave: Board.getGrave,
    BoardElement.Hand: Board.getHand,
    BoardElement.Field: Board.getField
}


ATTRIBUTE_GETTER = {
    AtrList.CardList: gameObj.cardList
}