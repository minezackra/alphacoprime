from enum import Enum, auto


class AbilityTag(Enum):
    Agent = auto()
    Spy = auto()

    RainMaker = auto()


class StatusTag(Enum):
    Revival = (auto(), "†")

    def __init__(self, val, string):
        self._value_ = val
        self._str = string

    def __str__(self):
        return self._str