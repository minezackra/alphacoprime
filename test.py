from game_element import Card, CardPile, GwentCard, Board, Player
from tag import Suit, CardType, Party
from ability import StatusTag
from deriver import BoardElement


c1 = Card(Suit.Heart, 1)
c2 = Card(Suit.Heart, 2)

gc1 = GwentCard(c1, CardType.UNIT, abilities=[StatusTag.Revival])
print([str(card) for card in Card.getInstances().values()])
print(gc1)
print(gc1.origCardId)

b1 = Board([Player(2,2), Player(2,2)], [CardPile(c1), CardPile(c2)], [], [], [])
be = BoardElement.Deck(Party.Self).CardList
print(be.instance(b1))
print([c1, c2])