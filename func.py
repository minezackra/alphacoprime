def iscoprime(a:int, b:int):
    m = min(a, b)
    if m == 1 or m == 0:
        return False
    for n in range(1, m+1):
        if (a % n == 0) and (b % n == 0) and n != 1:
            return False
    return True