from enum import Enum, IntEnum, auto


class Party(IntEnum):
    Self = 0
    Opponent = 1


class Color(Enum):
    Red = 0
    Black = 1


class Suit(Enum):
    Joker = (0, "Joker", None)
    Heart = (1, "♡", Color.Red)
    Spade = (2, "♠", Color.Black)
    Diamond = (3, "♢", Color.Red)
    Club = (4, "♣", Color.Black)

    def __init__(self, val: int, string: str, color: Color):
        self._value_ = val
        self._string = string
        self._color = color

    def __str__(self):
        return self._string

    @property
    def color(self):
        return self._color


class ElementType(Enum):
    Player = auto()
    
    Deck = auto()
    Grave = auto()
    Hand = auto()

    Field = auto()
    Row = auto()


class RowType(Enum):
    RowMelee = 1
    RowRanged = 2
    RowSiege = 3


class CardType(Enum):
    UNIT = auto()
    SPECIAL = auto()
    ARTIFACT = auto()